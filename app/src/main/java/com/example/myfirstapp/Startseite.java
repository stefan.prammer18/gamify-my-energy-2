package com.example.myfirstapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Startseite extends AppCompatActivity {

    public static final String SHARED_PREFS = "sharedPrefs";

    // ---------------------- Buttons -------------


    // ---------------------- Image-Buttons ------------------------
    private ImageButton sparschwein_button;
    private ImageButton verbrauch_button;
    private ImageButton quiz_button;
    private ImageButton global_button;
    private ImageButton profil_button;
    private ImageButton kosten_button;
    private ImageButton highscore_button;

    //------------------------- Variablen ------------------------
    private int profil_bild = 1;
    private int level = 1;
    private int Erfahrungspunkte = 0;
    private int ErfahrungspunkteInsgesamt = 0;
    private  int levelfueraufstieg = 50;
    private double gespart_in_euro = 0.00;
    private double gespart_verbrauch = 0.00;

    //------------------ TextViews ----------------------
    private TextView level_anzeige;
    private TextView level_fortschritt;


    private ProgressBar progressBar_xp;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startseite);

        //--------------------------- findViewById -------------------------
        profil_button = findViewById(R.id.button_profil);
        sparschwein_button = findViewById(R.id.button_sparschwein);
        verbrauch_button = findViewById(R.id.button_verbrauch);
        quiz_button = findViewById(R.id.button_quiz);
        global_button = findViewById(R.id.button_global);
        highscore_button = findViewById(R.id.button_highscore);
        kosten_button = findViewById(R.id.button_kosten);

        // ---------------------- ID-TextView -----------------
        level_anzeige = findViewById(R.id.textView_Level);
        level_fortschritt = findViewById(R.id.textView_lvl_fortschritt);
        progressBar_xp = findViewById(R.id.progressBar_lvl);

        loadData();

        //-------------------------- Ausfüllen der TextViews bei Aufruf -------------------
        progressBar_xp.setMax(levelfueraufstieg);
        progressBar_xp.setProgress(Erfahrungspunkte);
        level_fortschritt.setText(Erfahrungspunkte +"/"+levelfueraufstieg);
        level_anzeige.setText("Level: "+level);
        saveXp();

        // ------------------------- GET Intent ----------------------
        Intent intent = getIntent();
        profil_bild = intent.getIntExtra("Profil_zurück",1 );
        level = intent.getIntExtra("level",1);
        Erfahrungspunkte = intent.getIntExtra("Erfahrungspunkte",0);
        ErfahrungspunkteInsgesamt = intent.getIntExtra("ErfahrungspunkteInsgesamt",0);
        levelfueraufstieg = intent.getIntExtra("LevelfuerAufstieg",50);
        gespart_in_euro = intent.getDoubleExtra("Gespart_in_Euro",0.00);
        gespart_verbrauch = intent.getDoubleExtra("Gespart_Verbrauch", 0.00);
        //------------------------------ OnClickListener ---------------------------
        profil_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profiluebergabe();
            }
        });

        sparschwein_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sparschwein_uebergabe();
            }
        });

        verbrauch_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verbrauch_uebergabe();
            }
        });

        quiz_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quiz_uebergabe();
            }
        });

        kosten_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kosten_uebergabe();
            }
        });

        highscore_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                highscore_uebergabe();
            }
        });

        global_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                global_uebergabe();
            }
        });

    }

    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);
        Erfahrungspunkte = sharedPreferences.getInt("Erfahrungspunkte",0);
        ErfahrungspunkteInsgesamt = sharedPreferences.getInt("ErfahrungspunkteInsgesamt",0);
        levelfueraufstieg = sharedPreferences.getInt("levelfueraufstieg",50);
        level = sharedPreferences.getInt("level",1);

    }

    private void verbrauch_uebergabe() {
        startActivity(new Intent(this, Verbrauch.class));
    }

    // --------------------------- Übergabe zu Quiz-Seite -----------------------------------
    public void quiz_uebergabe(){
        Intent intent = new Intent(this,Quiz_seite.class);
        intent.putExtra("PROFILNUMMER",profil_bild);
        intent.putExtra("level",level);
        intent.putExtra("Erfahrungspunkte",Erfahrungspunkte);
        intent.putExtra("ErfahrungspunkteInsgesamt",ErfahrungspunkteInsgesamt);
        intent.putExtra("LevelfuerAufstieg",levelfueraufstieg);
        intent.putExtra("Gespart_in_Euro",gespart_in_euro);
        intent.putExtra("Gespart_Verbrauch", gespart_verbrauch);
        startActivity(intent);

    }

    // -------------------------------- Übergabe zu Profil-Seite ---------------
    public void profiluebergabe(){
        Intent intent = new Intent (this, Profil_seite.class);
        intent.putExtra("PROFILNUMMER",profil_bild);
        intent.putExtra("level",level);
        intent.putExtra("Erfahrungspunkte",Erfahrungspunkte);
        intent.putExtra("ErfahrungspunkteInsgesamt",ErfahrungspunkteInsgesamt);
        intent.putExtra("LevelfuerAufstieg",levelfueraufstieg);
        intent.putExtra("Gespart_in_Euro",gespart_in_euro);
        intent.putExtra("Gespart_Verbrauch", gespart_verbrauch);
        startActivity(intent);
    }

    // ----------------------------- Übergabe zu Sparschwein ---------------------------
    public void sparschwein_uebergabe(){
        Intent intent = new Intent (this, Sparschwein_seite.class);
        intent.putExtra("PROFILNUMMER",profil_bild);
        intent.putExtra("level",level);
        intent.putExtra("Erfahrungspunkte",Erfahrungspunkte);
        intent.putExtra("ErfahrungspunkteInsgesamt",ErfahrungspunkteInsgesamt);
        intent.putExtra("LevelfuerAufstieg",levelfueraufstieg);
        intent.putExtra("Gespart_in_Euro",gespart_in_euro);
        intent.putExtra("Gespart_Verbrauch", gespart_verbrauch);
        startActivity(intent);
    }

    // ---------------------------- Highscore-Übergabe ---------------------
    public void highscore_uebergabe(){
        Intent intent = new Intent (this, highscore_seite.class);
        intent.putExtra("PROFILNUMMER",profil_bild);
        intent.putExtra("level",level);
        intent.putExtra("Erfahrungspunkte",Erfahrungspunkte);
        intent.putExtra("ErfahrungspunkteInsgesamt",ErfahrungspunkteInsgesamt);
        intent.putExtra("LevelfuerAufstieg",levelfueraufstieg);
        intent.putExtra("Gespart_in_Euro",gespart_in_euro);
        intent.putExtra("Gespart_Verbrauch", gespart_verbrauch);
        startActivity(intent);
    }

    // ----------------------------- Übergabe zu Global ---------------------------
    public void global_uebergabe(){
        Intent intent = new Intent (this, global_seite.class);
        intent.putExtra("PROFILNUMMER",profil_bild);
        intent.putExtra("level",level);
        intent.putExtra("Erfahrungspunkte",Erfahrungspunkte);
        intent.putExtra("ErfahrungspunkteInsgesamt",ErfahrungspunkteInsgesamt);
        intent.putExtra("LevelfuerAufstieg",levelfueraufstieg);
        intent.putExtra("Gespart_in_Euro",gespart_in_euro);
        intent.putExtra("Gespart_Verbrauch", gespart_verbrauch);
        startActivity(intent);
    }


    //----------------------Kostenübergabe ---------------------------
    public void kosten_uebergabe(){
        Intent intent = new Intent (this, Kosten_seite.class);
        intent.putExtra("PROFILNUMMER",profil_bild);
        intent.putExtra("level",level);
        intent.putExtra("Erfahrungspunkte",Erfahrungspunkte);
        intent.putExtra("ErfahrungspunkteInsgesamt",ErfahrungspunkteInsgesamt);
        intent.putExtra("LevelfuerAufstieg",levelfueraufstieg);
        intent.putExtra("Gespart_in_Euro",gespart_in_euro);
        intent.putExtra("Gespart_Verbrauch", gespart_verbrauch);
        startActivity(intent);
    }

    private void saveXp(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("Erfahrungspunkte",Erfahrungspunkte);
        editor.putInt("level",level);
        editor.putInt("levelfueraufstieg", levelfueraufstieg);
        editor.apply();
    }


    //--------------------------- Wenn der Zurückbutton geklickt wird -------
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);

    }
}
