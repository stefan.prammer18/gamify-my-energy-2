package com.example.myfirstapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class global_seite extends AppCompatActivity {

    //---------------------Variablen ---------------------
    private double kosten = 18900;
    private double verbrauch = 72589;
    private float gespart_in_euro;
    private float gespart_verbrauch;
    private TextView kosten_anzeige;
    private TextView verbrauch_anzeige;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_seite);

        kosten_anzeige =findViewById(R.id.kosten_insert);
        verbrauch_anzeige =findViewById(R.id.verbrauch_insert);

        loadData();

        verbrauch += Double.parseDouble(Float.valueOf(gespart_verbrauch).toString());
        kosten += Double.parseDouble(Float.valueOf(gespart_in_euro).toString());

        String kosten_s = Double.toString(kosten);
        String verbrauch_s = Double.toString(verbrauch);

        kosten_anzeige.setText(kosten_s +" €");
        verbrauch_anzeige.setText(verbrauch_s+" kw/h");

    }

    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences("sharedPrefs",MODE_PRIVATE);
        gespart_verbrauch = sharedPreferences.getFloat("gespart_verbrauch",0);
        gespart_in_euro = sharedPreferences.getFloat("gespart_in_euro",0);
    }

   @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, Startseite.class);
        startActivity(intent);
        super.onBackPressed();
    }


}
