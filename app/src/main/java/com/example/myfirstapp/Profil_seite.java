package com.example.myfirstapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Profil_seite extends AppCompatActivity {



    public static final String SHARED_PREFS = "sharedPrefs";

    //-----------------Buttons--------------
    private Button plus_profil;
    private Button minus_profil;
    private Button plus_xp;
    private Button share_button;
    private Button delete_button;

    private ImageView profil_image;

    //------------------ TextViews ----------------------
    private TextView ausgabe_bildnummer;
    private TextView level_anzeige;
    private TextView level_fortschritt;
    private TextView level_insgesamt;
    private TextView gesamt_gespart;
    private TextView user_name;
    private TextView kw_gespart;

    private ProgressBar progressBar_xp;

    //---------------------Variablen ---------------------
    private int ErfahrungspunkteInsgesamt = 0;
    private int Erfahrungspunkte = 0;
    private int image_number = 0;
    private int level = 1;
    private int levelfueraufstieg = 50; //Um die Progressbar-Länge je nach Level zu erhöhen.
    private double gespart_in_euro =0.00;
    private float gespart_in_euro_f = 0.00f;
    private double gespart_verbrauch = 0.00;
    private float gespart_verbrauch_f = 0.00f;
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_seite);
        profil_image = findViewById(R.id.imageView_profil);

        // -------------------- ID-Button-------------------
        plus_profil =findViewById(R.id.button_plus_profil);
        minus_profil = findViewById(R.id.button_minus_profil);
        plus_xp = findViewById(R.id.button_lvl_plus);
        share_button = findViewById(R.id.button_share);
        delete_button = findViewById(R.id.button_löschen);

        // ---------------------- ID-TextView -----------------
        ausgabe_bildnummer = findViewById(R.id.Ausgabe_Bildnummer);
        level_anzeige = findViewById(R.id.textView_Level);
        level_fortschritt = findViewById(R.id.textView_lvl_fortschritt);
        level_insgesamt = findViewById(R.id.textView_Insgesamt_xp);
        gesamt_gespart = findViewById(R.id.textView_gespart_insgesamt);
        progressBar_xp = findViewById(R.id.progressBar_lvl);
        user_name = findViewById(R.id.textView_user_name);
        kw_gespart = findViewById(R.id.textView_kw);


        loadData();

        //-------------------------- Ausfüllen der TextViews bei Aufruf -------------------
        progressBar_xp.setMax(levelfueraufstieg);
        progressBar_xp.setProgress(Erfahrungspunkte);
        level_fortschritt.setText(Erfahrungspunkte +"/"+levelfueraufstieg);
        level_anzeige.setText("Level: "+level);
        level_insgesamt.setText(ErfahrungspunkteInsgesamt+" xp");
        gesamt_gespart.setText(String.format("%.2f", gespart_in_euro_f)+" €");
        user_name.setText(username);
        kw_gespart.setText(gespart_verbrauch_f+" kw/h");

        switch(image_number) {
            case 0:
                profil_image.setImageResource(R.drawable.ic_avataaars);
                break;
            case 1:
                profil_image.setImageResource(R.drawable.ic_adrian_mahler);
                break;
            case 2:
                profil_image.setImageResource(R.drawable.ic_anna);
                break;
            case 3:
                profil_image.setImageResource(R.drawable.ic_herbert);
                break;
            case 4:
                profil_image.setImageResource(R.drawable.ic_johannes_seiberl);
                break;
            case 5:
                profil_image.setImageResource(R.drawable.ic_avatar_1);
                break;
            case 6:
                profil_image.setImageResource(R.drawable.ic_avatar_2);
                break;
            case 7:
                profil_image.setImageResource(R.drawable.ic_avatar_3);
                break;
            case 8:
                profil_image.setImageResource(R.drawable.ic_avatar_4);
                break;
            case 9:
                profil_image.setImageResource(R.drawable.ic_avatar_5);
                break;
        }
        //ausgabe_bildnummer.setText(""+image_number); //zum Kontrollieren die Zummer des Switches

        // -------------------- Hier ist der Button für Plus Profilbild -----------------------------
        plus_profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(image_number !=9)
                    image_number++;
                else
                    image_number = 0;

                switch(image_number) {
                    case 0:
                        profil_image.setImageResource(R.drawable.ic_avataaars);
                        break;
                    case 1:
                        profil_image.setImageResource(R.drawable.ic_adrian_mahler);
                        break;
                    case 2:
                        profil_image.setImageResource(R.drawable.ic_anna);
                        break;
                    case 3:
                        profil_image.setImageResource(R.drawable.ic_herbert);
                        break;
                    case 4:
                        profil_image.setImageResource(R.drawable.ic_johannes_seiberl);
                        break;
                    case 5:
                        profil_image.setImageResource(R.drawable.ic_avatar_1);
                        break;
                    case 6:
                        profil_image.setImageResource(R.drawable.ic_avatar_2);
                        break;
                    case 7:
                        profil_image.setImageResource(R.drawable.ic_avatar_3);
                        break;
                    case 8:
                        profil_image.setImageResource(R.drawable.ic_avatar_4);
                        break;
                    case 9:
                        profil_image.setImageResource(R.drawable.ic_avatar_5);
                        break;
                }
                saveBild(image_number);
                //ausgabe_bildnummer.setText(""+image_number); //zum Kontrollieren die Zummer des Switches
            }
        });

        // ------------------ Hier ist der Minusbutton für das Profilbild ------------------------
        minus_profil.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (image_number != 0)
                    image_number--;
                else
                    image_number = 9;

                switch (image_number) {
                    case 0:
                        profil_image.setImageResource(R.drawable.ic_avataaars);
                        break;
                    case 1:
                        profil_image.setImageResource(R.drawable.ic_adrian_mahler);
                        break;
                    case 2:
                        profil_image.setImageResource(R.drawable.ic_anna);
                        break;
                    case 3:
                        profil_image.setImageResource(R.drawable.ic_herbert);
                        break;
                    case 4:
                        profil_image.setImageResource(R.drawable.ic_johannes_seiberl);
                        break;
                    case 5:
                        profil_image.setImageResource(R.drawable.ic_avatar_1);
                        break;
                    case 6:
                        profil_image.setImageResource(R.drawable.ic_avatar_2);
                        break;
                    case 7:
                        profil_image.setImageResource(R.drawable.ic_avatar_3);
                        break;
                    case 8:
                        profil_image.setImageResource(R.drawable.ic_avatar_4);
                        break;
                    case 9:
                        profil_image.setImageResource(R.drawable.ic_avatar_5);
                        break;
                }
                saveBild(image_number);
                //ausgabe_bildnummer.setText(""+image_number); //zum Kontrollieren die Zummer des Switches
            }
        });

        //---------------------------- Hier der +10 xp Button -----------------
        plus_xp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Erfahrungspunkte += 10;
                ErfahrungspunkteInsgesamt += 10;
                if(Erfahrungspunkte >= levelfueraufstieg){
                    Erfahrungspunkte -= levelfueraufstieg;
                    levelfueraufstieg +=50;
                    level++;
                }
                level_anzeige.setText("Level: "+level);
                progressBar_xp.setMax(levelfueraufstieg);
                progressBar_xp.setProgress(Erfahrungspunkte);
                level_fortschritt.setText(Erfahrungspunkte +"/"+levelfueraufstieg);
                level_insgesamt.setText(ErfahrungspunkteInsgesamt+" xp");
                saveXp();
            }
        });

        //---------------------------- Hier der Share-Button -----------------
        share_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/plain");
                String shareBody = "Ich habe mit der App Energy Trainer " + String.format("%.2f",gespart_in_euro) + "€ und " + String.format("%.2f",gespart_verbrauch) + " kw/h gespart!";
                String shareSub = "Einsparungen";
                myIntent.putExtra(Intent.EXTRA_SUBJECT, shareBody);
                myIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(myIntent, "Share using"));
            }

        });

        //------------------------------- Hier der Delete-Button -------------------------
        delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickDelete();
            }
        });

    }

    private void deleteData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("ErfahrungspunkteInsgesamt",0);
        editor.putInt("Erfahrungspunkte",0);
        editor.putInt("level",1);
        editor.putInt("levelfueraufstieg", 50);
        editor.putFloat("gespart_in_euro_f", 0.00f);
        editor.putFloat("gespart_verbrauch_f",0.00f);
        editor.putInt("PROFILFOTO",0);
        editor.putBoolean("firstVisit",true);
        editor.apply();
    }

    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);
        image_number = sharedPreferences.getInt("PROFILFOTO",0);
        Erfahrungspunkte = sharedPreferences.getInt("Erfahrungspunkte",0);
        ErfahrungspunkteInsgesamt = sharedPreferences.getInt("ErfahrungspunkteInsgesamt",0);
        levelfueraufstieg = sharedPreferences.getInt("levelfueraufstieg",50);
        level = sharedPreferences.getInt("level",1);
        gespart_in_euro_f = sharedPreferences.getFloat("gespart_in_euro_f",0.00f);
        gespart_verbrauch_f = sharedPreferences.getFloat("gespart_verbrauch_f",0.00f);
        username = sharedPreferences.getString("username","me");

    }

    private void clickDelete(){
       // final boolean ausgabe = false;
        AlertDialog.Builder alert = new AlertDialog.Builder((this));
        alert.setTitle("Löschen?");
        alert.setMessage("Möchtest du wirklich alle Daten löschen?");
        alert.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                deleteData();
                loadFirstVisit();

            }
        });
        alert.setNegativeButton("Nein", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alert.create().show();
    }

    private void loadFirstVisit(){
        startActivity(new Intent(this, activity_first_visit.class));
    }

    private void saveBild(int a){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("PROFILFOTO",a);

        editor.apply();
    }

    private void saveXp(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("ErfahrungspunkteInsgesamt",ErfahrungspunkteInsgesamt);
        editor.putInt("Erfahrungspunkte",Erfahrungspunkte);
        editor.putInt("level",level);
        editor.putInt("levelfueraufstieg", levelfueraufstieg);
        editor.apply();
    }



    //---------------------- Wenn der Backbutten gedrückt wird. ------------------------
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,Startseite.class);
        startActivity(intent);
        super.onBackPressed();
    }
}
