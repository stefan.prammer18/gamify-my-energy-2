package com.example.myfirstapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;


public class Sparschwein_seite extends AppCompatActivity {

    //----------- Random -----------------
    Random rnd_aufgabe = new Random();

    //------------- Class Aufgaben ------------
    class Aufgabe{
        String text;
        int xp;
        double ersparnis;
        double kostenersparnis;

        Aufgabe(String aufgabentext,int Wie_viel_xp,double Wie_viel_Ersparnis, double kostensparen){
            text = aufgabentext;
            xp = Wie_viel_xp;
            ersparnis = Wie_viel_Ersparnis;
            kostenersparnis = kostensparen;
        }
        public int getXp(){
            return xp;
        }
        public String getText(){
            return text;
        }
        public double getErsparnis(){
            return ersparnis;
        }
        public double getKostenersparnis(){
            return kostenersparnis;
        }
    }


    ArrayList<Aufgabe> aufgaben = new ArrayList<>();

    //---------------- Aufgaben --------------------------
    Aufgabe aufgabe1 = new Aufgabe("Dreh den Fernseher ab und nicht nur auf Standby", 10, 0.33, 0.05);
    Aufgabe aufgabe2 = new Aufgabe("Verwende das Energieprogramm der Waschmaschine", 10, 0.93, 0.16);
    Aufgabe aufgabe3 = new Aufgabe("Mach den Wasserkocher nicht zu voll", 10, 0.16, 0.03);
    Aufgabe aufgabe4 = new Aufgabe("Verzichte auf die Klimaanlage", 10, 1.20, 0.20);
    Aufgabe aufgabe5 = new Aufgabe("Verwende einen Laptop anstelle eines Stand-PCs", 10, 0.88, 0.15);
    Aufgabe aufgabe6 = new Aufgabe("Taue die Kühlgeräte regelmäßig ab", 10, 0.24, 0.04);



    //------------------ Variablen --------------------
    private int ErfahrungspunkteInsgesamt = 0;
    private int Erfahrungspunkte = 0;
    private int image_number = 0;
    private int level = 1;
    private int levelfueraufstieg = 50; //Um die Progressbar-Länge je nach Level zu erhöhen.
    private double gespart_in_euro;
    private double gespart_verbrauch;

   // private float gespart_in_euro_f = 0.00f;
  //  private float gespart_verbrauch_f = 0.00f;

    //------------------ Variablen für Aufgaben --------------------
    private Aufgabe ausgabe_aufgabe_1;
    private Aufgabe ausgabe_aufgabe_2;
    private Aufgabe ausgabe_aufgabe_3;
    private int idx_ausgabe_aufgabe_1;
    private int idx_ausgabe_aufgabe_2;
    private int idx_ausgabe_aufgabe_3;

    //----------------------- Buttons ----------------------
    private Button Button_Aufgabe1;
    private Button Button_Aufgabe2;
    private Button Button_Aufgabe3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sparschwein_seite);
        aufgaben.add(aufgabe1);
        aufgaben.add(aufgabe2);
        aufgaben.add(aufgabe3);
        aufgaben.add(aufgabe4);
        aufgaben.add(aufgabe5);
        aufgaben.add(aufgabe6);




        idx_ausgabe_aufgabe_1 = rnd_aufgabe.nextInt(aufgaben.size());
        do{idx_ausgabe_aufgabe_2 = rnd_aufgabe.nextInt(aufgaben.size());}
        while (idx_ausgabe_aufgabe_2 == idx_ausgabe_aufgabe_1);
        do{idx_ausgabe_aufgabe_3 = rnd_aufgabe.nextInt(aufgaben.size());}
        while (idx_ausgabe_aufgabe_3 == idx_ausgabe_aufgabe_1 || idx_ausgabe_aufgabe_3 == idx_ausgabe_aufgabe_2);

        ausgabe_aufgabe_1 = aufgaben.get(idx_ausgabe_aufgabe_1);
        ausgabe_aufgabe_2 = aufgaben.get(idx_ausgabe_aufgabe_2);
        ausgabe_aufgabe_3 = aufgaben.get(idx_ausgabe_aufgabe_3);



        // ------------------------- findViewByID -----------------
        Button_Aufgabe1 = findViewById(R.id.button_Aufgabe_1);
        Button_Aufgabe2 = findViewById(R.id.button_Aufgabe_2);
        Button_Aufgabe3 = findViewById(R.id.button_Aufgabe_3);

        loadData();



        Button_Aufgabe1.setText(ausgabe_aufgabe_1.text + "\n" + ausgabe_aufgabe_1.getErsparnis()+" kw/h | "+String.format("%.2f", ausgabe_aufgabe_1.getKostenersparnis())+" €");
        Button_Aufgabe2.setText(ausgabe_aufgabe_2.text + "\n" + ausgabe_aufgabe_2.getErsparnis()+" kw/h | "+String.format("%.2f", ausgabe_aufgabe_2.getKostenersparnis())+" €");
        Button_Aufgabe3.setText(ausgabe_aufgabe_3.text + "\n" + ausgabe_aufgabe_3.getErsparnis()+" kw/h | "+String.format("%.2f", ausgabe_aufgabe_3.getKostenersparnis())+" €");

        Button_Aufgabe1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int old_idx = idx_ausgabe_aufgabe_1;
                aufgabe_abschluss(ausgabe_aufgabe_1);
                do{idx_ausgabe_aufgabe_1 = rnd_aufgabe.nextInt(aufgaben.size());}
                while (idx_ausgabe_aufgabe_1 == idx_ausgabe_aufgabe_2 || idx_ausgabe_aufgabe_1 == idx_ausgabe_aufgabe_3 || idx_ausgabe_aufgabe_1 == old_idx);
                ausgabe_aufgabe_1 = aufgaben.get(idx_ausgabe_aufgabe_1);
                Button_Aufgabe1.setText(ausgabe_aufgabe_1.text + "\n" + ausgabe_aufgabe_1.getErsparnis()+" kw/h | "+String.format("%.2f", ausgabe_aufgabe_1.getKostenersparnis())+" €");
            }
        });

        Button_Aufgabe2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int old_idx = idx_ausgabe_aufgabe_2;
                aufgabe_abschluss(ausgabe_aufgabe_2);
                do{idx_ausgabe_aufgabe_2 = rnd_aufgabe.nextInt(aufgaben.size());}
                while (idx_ausgabe_aufgabe_2 == idx_ausgabe_aufgabe_1 || idx_ausgabe_aufgabe_2 == idx_ausgabe_aufgabe_3 ||idx_ausgabe_aufgabe_2 == old_idx);
                ausgabe_aufgabe_2 = aufgaben.get(idx_ausgabe_aufgabe_2);
                Button_Aufgabe2.setText(ausgabe_aufgabe_2.text + "\n" + ausgabe_aufgabe_2.getErsparnis()+" kw/h | "+String.format("%.2f", ausgabe_aufgabe_2.getKostenersparnis())+" €");
            }
        });

        Button_Aufgabe3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int old_idx = idx_ausgabe_aufgabe_3;
                aufgabe_abschluss(ausgabe_aufgabe_3);
                do{idx_ausgabe_aufgabe_3 = rnd_aufgabe.nextInt(aufgaben.size());}
                while (idx_ausgabe_aufgabe_3 == idx_ausgabe_aufgabe_1 || idx_ausgabe_aufgabe_3 == idx_ausgabe_aufgabe_2 || idx_ausgabe_aufgabe_3 == old_idx);
                ausgabe_aufgabe_3 = aufgaben.get(idx_ausgabe_aufgabe_3);
                Button_Aufgabe3.setText(ausgabe_aufgabe_3.text + "\n" + ausgabe_aufgabe_3.getErsparnis()+" kw/h | "+String.format("%.2f", ausgabe_aufgabe_3.getKostenersparnis())+" €");
            }
        });
    }

    // ---------- Beim Buttonklick der 3 Buttons ----------------
    private void aufgabe_abschluss(Aufgabe a){
        Erfahrungspunkte += a.getXp();
        if(Erfahrungspunkte >= levelfueraufstieg){

            Erfahrungspunkte -=levelfueraufstieg;
            levelfueraufstieg+=50;
            level++;
        }
        ErfahrungspunkteInsgesamt += a.getXp();
        gespart_in_euro += (float) a.getKostenersparnis();
        gespart_verbrauch += (float) a.getErsparnis();
        Toast toast = Toast.makeText(this,"+" +a.getXp() +" xp",Toast.LENGTH_SHORT);
        toast.show();
        saveData();
    }

    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences("sharedPrefs",MODE_PRIVATE);
        Erfahrungspunkte = sharedPreferences.getInt("Erfahrungspunkte",0);
        ErfahrungspunkteInsgesamt = sharedPreferences.getInt("ErfahrungspunkteInsgesamt",0);
        levelfueraufstieg = sharedPreferences.getInt("levelfueraufstieg",50);
        level = sharedPreferences.getInt("level",1);
        gespart_in_euro = sharedPreferences.getFloat("gespart_in_euro_f",0.00f);
        gespart_verbrauch = sharedPreferences.getFloat("gespart_verbrauch_f",0.00f);
    }

    private void saveData(){
            SharedPreferences sharedPreferences = getSharedPreferences("sharedPrefs",MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("ErfahrungspunkteInsgesamt",ErfahrungspunkteInsgesamt);
            editor.putInt("Erfahrungspunkte",Erfahrungspunkte);
            editor.putInt("level",level);
            editor.putInt("levelfueraufstieg", levelfueraufstieg);
            editor.putFloat("gespart_in_euro_f", (float) gespart_in_euro);
            editor.putFloat("gespart_verbrauch_f",(float) gespart_verbrauch);
            editor.apply();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,Startseite.class);
        startActivity(intent);
        super.onBackPressed();
    }
}
