package com.example.myfirstapp;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.os.Handler; // noch Fragen
import android.content.Intent;
import java.util.Random;    // Random Fragen

public class MainActivity extends AppCompatActivity {

    private ProgressBar progressBar_loading;
    private TextView fin_loading;
    private String tipps [] = {"Tipp 1: Schalte deine Elektrogeräte aus, anstatt sie im Standby-Modus zu lassen.",
            "Tipp 2: Schalte das Licht aus, wenn du den Raum verlässt.","Tipp 3: Verwende beim Kochen einen Deckel.","Tipp 4: Taue deine Kühlgeräte gelegentlich ab.",
            "Tipp 5: Wasche deine Wäsche bei niedrigen Temperaturen.","Tipp 6: Trockne deine Wäsche auf einem Wäscheständer, anstatt den Trockner zu verwenden."
            ,"Tipp 7: Nutze die Sparprogramme deiner Waschmaschine.","Tipp 8: Reize die Nutzungsdauer deiner Elektrogeräte aus.","Tipp 9: Investiere in Geräte mit der höchsten Energieeffizienzklasse.",
            "Tipp 10: Verwende LED-Lampen und Energiesparlampen anstelle von herkömmlichen Glühbirnen."};

    //----------- Random ---------------
    Random tipp_random = new Random();

    private boolean firstVisit;
    private int profil_foto;
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String FIRSTVISIT = "firstVisit";

    private int progress = 0;

    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar_loading = findViewById(R.id.progressBar_loading);
        fin_loading = findViewById(R.id.Spartipps);

        loadData();

        fin_loading.setText(tipps[tipp_random.nextInt(10)]);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progress <100) {
                    progress++;
                    android.os.SystemClock.sleep(50);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar_loading.setProgress(progress);
                        }
                    });

                }
                if(firstVisit){
                    startActivity(new Intent(MainActivity.this,activity_first_visit.class));
                    //startActivity(new Intent(MainActivity.this, Startseite.class));

                }
                else {
                    startActivity(new Intent(MainActivity.this, Startseite.class));
                }
            }
        }).start();

    }
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        super.onBackPressed();
    }

    public void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);
        firstVisit = sharedPreferences.getBoolean(FIRSTVISIT,true);
        profil_foto = sharedPreferences.getInt("PROFILFOTO", 0);
    }
}
