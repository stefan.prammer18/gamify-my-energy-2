package com.example.myfirstapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class activity_first_visit extends AppCompatActivity {
    //--------- EditTextViews------
    private EditText wasser_verbrauch;
    private EditText gas_verbrauch;
    private EditText strom_verbrauch;
    private EditText heizung_verbrauch;
    private EditText wasser_kosten;
    private EditText gas_kosten;
    private EditText heizung_kosten;
    private EditText strom_kosten;
    private EditText username;

    //----Buttons ---------
    private Button start;
    private CheckBox default_values;

    //----Strings----
    private String user;

    //----------Floatvariablen zum Übergeben----------
    private float gesamtverbrauch;
    private float gesamtkosten;
    private float wasserverbrauch;
    private float wasserkosten;
    private float stromkosten;
    private float stromverbrauch;
    private float gaskosten;
    private float gasverbrauch;
    private float heizungskosten;
    private float heizungsverbrauch;

    //-------- EXTRA Variables -------------

    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String VERBRAUCH = "verbrauch";
    public static final String WASSERVERBRAUCH = "wasserverbrauch";
    public static final String GASVERBRAUCH = "gasverbrauch";
    public static final String STROMVERBRAUCH = "stromverbrauch";
    public static final String HEIZUNGVERBRAUCH = "heizungverbrauch";
    public static final String KOSTEN = "kosten";
    public static final String WASSERKOSTEN = "wasserkosten";
    public static final String GASKOSTEN = "gaskosten";
    public static final String HEIZUNGKOSTEN = "heizungkosten";
    public static final String STROMKOSTEN = "stromkosten";
    public static final String USER = "username";
    public static final String FIRSTVISIT = "firstVisit";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_visit);
        //------------- Views initialisieren -------------
        wasser_verbrauch = findViewById(R.id.wasser_verbrauch);
        wasser_kosten= findViewById(R.id.kosten_wasser);
        heizung_verbrauch = findViewById(R.id.heizung_verbrauch);
        heizung_kosten= findViewById(R.id.kosten_heizung);
        strom_verbrauch = findViewById(R.id.strom_verbrauch);
        strom_kosten= findViewById(R.id.kosten_strom);
        gas_verbrauch = findViewById(R.id.gas_verbrauch);
        gas_kosten= findViewById(R.id.kosten_gas);
        username = findViewById(R.id.username);
        start = findViewById(R.id.button_start);
        default_values = findViewById(R.id.default_values);


        //------ Gesamtverbrauch + -kosten berechnen
        gesamtverbrauch = wasserverbrauch + stromverbrauch + gasverbrauch + heizungsverbrauch;
        gesamtkosten = wasserkosten + stromkosten + gaskosten + heizungskosten;

        default_values.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    wasser_kosten.setFocusable(false);
                    wasser_kosten.setText("170");
                    wasser_verbrauch.setFocusable(false);
                    wasser_verbrauch.setText("1000");
                    strom_verbrauch.setFocusable(false);
                    strom_verbrauch.setText("4000");
                    strom_kosten.setFocusable(false);
                    strom_kosten.setText("680");
                    gas_verbrauch.setFocusable(false);
                    gas_verbrauch.setText("8560");
                    gas_kosten.setFocusable(false);
                    gas_kosten.setText("1455.2");
                    heizung_verbrauch.setFocusable(false);
                    heizung_verbrauch.setText("14000");
                    heizung_kosten.setFocusable(false);
                    heizung_kosten.setText("2380");
                }
                else {
                    wasser_kosten.setFocusableInTouchMode(true);
                    wasser_verbrauch.setFocusableInTouchMode(true);
                    strom_verbrauch.setFocusableInTouchMode(true);
                    strom_kosten.setFocusableInTouchMode(true);
                    gas_verbrauch.setFocusableInTouchMode(true);
                    gas_kosten.setFocusableInTouchMode(true);
                    heizung_verbrauch.setFocusableInTouchMode(true);
                    heizung_kosten.setFocusableInTouchMode(true);
                }
            }
        });

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (default_values.isChecked()) {
                    user = "User";
                    wasserverbrauch = 1000;
                    wasserkosten = 170;
                    stromverbrauch = 4000;
                    stromkosten = 680;
                    gasverbrauch = 8560;
                    gaskosten = (float) 1455.20;
                    heizungsverbrauch = 14000;
                    heizungskosten = 2380;
                }
                else {
                    //---------- Werte in Variablen speichern -----------
                    String wasservtemp = wasser_verbrauch.getText().toString();
                    String wasserktemp = wasser_kosten.getText().toString();
                    String gasvtemp = gas_verbrauch.getText().toString();
                    String gasktemp = gas_kosten.getText().toString();
                    String heizungvtemp = heizung_verbrauch.getText().toString();
                    String heizungktemp = heizung_kosten.getText().toString();
                    String stromktemp = strom_kosten.getText().toString();
                    String stromvtemp = strom_verbrauch.getText().toString();
                    user = username.getText().toString();

                    if (!(wasservtemp.isEmpty())) {
                        wasserverbrauch = Float.parseFloat(wasservtemp);
                    }
                    if (!(wasserktemp.isEmpty())) {
                        wasserkosten = Float.parseFloat(wasserktemp);
                    }
                    if (!(gasvtemp.isEmpty())) {
                        gasverbrauch = Float.parseFloat(gasvtemp);
                    }
                    if (!(gasktemp.isEmpty())) {
                        gaskosten = Float.parseFloat(gasktemp);
                    }
                    if (!(heizungvtemp.isEmpty())) {
                        heizungsverbrauch = Float.parseFloat(heizungvtemp);
                    }
                    if (!(heizungktemp.isEmpty())) {
                        heizungskosten = Float.parseFloat(heizungktemp);
                    }
                    if (!(stromktemp.isEmpty())) {
                        stromkosten = Float.parseFloat(stromktemp);
                    }
                    if (!(stromvtemp.isEmpty())) {
                        stromverbrauch = Float.parseFloat(stromvtemp);
                    }
                }
                //----------Wenn Standardwerte verwendet werden sollen -------------


                    saveData();
                    start_uebergabe();
            }
        });
    }


    private void start_uebergabe() {
        startActivity(new Intent(this, Startseite.class));
    }

    public void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(WASSERKOSTEN, wasserkosten);
        editor.putFloat(WASSERVERBRAUCH, wasserverbrauch);
        editor.putFloat(STROMKOSTEN, stromkosten);
        editor.putFloat(STROMVERBRAUCH, stromverbrauch);
        editor.putFloat(GASVERBRAUCH, gasverbrauch);
        editor.putFloat(GASKOSTEN, gaskosten);
        editor.putFloat(HEIZUNGKOSTEN, heizungskosten);
        editor.putFloat(HEIZUNGVERBRAUCH, heizungsverbrauch);
        editor.putFloat(VERBRAUCH, gesamtverbrauch);
        editor.putFloat(KOSTEN, gesamtkosten);
        editor.putString(USER, username.getText().toString());
        editor.putBoolean(FIRSTVISIT, false);

        editor.apply();
    }

    //--------------------------- Wenn der Zurückbutton geklickt wird -------
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);

    }
}