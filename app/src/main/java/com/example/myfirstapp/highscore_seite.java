package com.example.myfirstapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;

public class highscore_seite extends AppCompatActivity {


    class User{
        String name;
        int xp;

        User(String Name, int Xp){
            name = Name;
            xp = Xp;
        }

        public String getName(){
            return name;
        }
        public int getXp(){
            return xp;
        }
    }

    // ---------------------- User-Ranking, sollte von einer Datenbankabfrage kommen und die User in Absteigender Reihenfolge anzeigen ----------------
    ArrayList<User> User_ranking = new ArrayList<>();

    User katrin = new User("Katrin",1000);
    User stefan = new User("Stefan", 900);
    User alex = new User("Alex",800);
    User herbert = new User("Herbert",700);
    User anna = new User("Anna",600);
    User johannes = new User("Johannes",500);
    User adrian = new User ("Adrian",400);

    //---------------------Variablen ---------------------
    private int ErfahrungspunkteInsgesamt = 0;
    private String name="Me";

    // ---------------------- TextView ------------------
    private TextView rang1_name;
    private TextView rang1_xp;
    private TextView rang2_name;
    private TextView rang2_xp;
    private TextView rang3_name;
    private TextView rang3_xp;
    private TextView rang4_name;
    private TextView rang4_xp;
    private TextView rang5_name;
    private TextView rang5_xp;
    private TextView rangme_nr;
    private TextView rangme_name;
    private TextView rangme_xp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore_seite);

        //------------ add User to ArrayList -----------------
        User_ranking.add(katrin);
        User_ranking.add(stefan);
        User_ranking.add(alex);
        User_ranking.add(herbert);
        User_ranking.add(anna);
        User_ranking.add(johannes);
        User_ranking.add(adrian);

        // ----------------------- findViewById ------------------------
        rang1_name = findViewById(R.id.textView_rang1_name);
        rang1_xp = findViewById(R.id.textView_rang1_xp);
        rang2_name = findViewById(R.id.textView_rang2_name);
        rang2_xp = findViewById(R.id.textView_rang2_xp);
        rang3_name = findViewById(R.id.textView_rang3_name);
        rang3_xp = findViewById(R.id.textView_rang3_xp);
        rang4_name = findViewById(R.id.textView_rang4_name);
        rang4_xp = findViewById(R.id.textView_rang4_xp);
        rang5_name = findViewById(R.id.textView_rang5_name);
        rang5_xp = findViewById(R.id.textView_rang5_xp);
        rangme_nr = findViewById(R.id.textView_rangyou);
        rangme_name = findViewById(R.id.textView_rangyou_name);
        rangme_xp = findViewById(R.id.textView_rangyou_xp);

        loadData();

        User me = new User(name,ErfahrungspunkteInsgesamt);

        int pos_me = User_ranking.size()-1;
        while(ErfahrungspunkteInsgesamt > User_ranking.get(pos_me).getXp()){
                pos_me--;
                if(pos_me == -1)
                    break;
        }

            pos_me++;
        User_ranking.add(pos_me,me);
        rang1_name.setText(User_ranking.get(0).getName());
        rang1_xp.setText(User_ranking.get(0).getXp()+" xp");
        rang2_name.setText(User_ranking.get(1).getName());
        rang2_xp.setText(User_ranking.get(1).getXp()+" xp");
        rang3_name.setText(User_ranking.get(2).getName());
        rang3_xp.setText(User_ranking.get(2).getXp()+" xp");
        rang4_name.setText(User_ranking.get(3).getName());
        rang4_xp.setText(User_ranking.get(3).getXp()+" xp");
        rang5_name.setText(User_ranking.get(4).getName());
        rang5_xp.setText(User_ranking.get(4).getXp()+" xp");
        if(pos_me+1>5){
            rangme_nr.setText(pos_me+1+".");
            rangme_name.setText(User_ranking.get(pos_me).getName());
            rangme_xp.setText(User_ranking.get(pos_me).getXp()+" xp");
        }
    }


    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences("sharedPrefs",MODE_PRIVATE);
        ErfahrungspunkteInsgesamt = sharedPreferences.getInt("ErfahrungspunkteInsgesamt",0);
        name = sharedPreferences.getString("username","Me");
    }
}
