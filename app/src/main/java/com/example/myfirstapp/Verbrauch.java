package com.example.myfirstapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;


import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.Calendar;

import java.util.ArrayList;

public class Verbrauch extends AppCompatActivity {


    private Button minus_button;

    // Verbrauchswerte pro Tag in kw/h
    private float verbrauch_mo = 945f;
    private float verbrauch_di = 1040f;
    private float verbrauch_mi = 1133f;
    private float verbrauch_do = 1240f;
    private float verbrauch_fr = 1369f;
    private float verbrauch_sa = 1487f;
    private float verbrauch_so = 1501f;

    //Wert um den der Verbrauch verringert wird
    private float value = 100f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Calendar calendar = Calendar.getInstance();
        final int curr_day = calendar.get(Calendar.DAY_OF_WEEK);
        setContentView(R.layout.activity_verbrauch);

        minus_button = findViewById(R.id.button_minus);

        // Verbrauch verringern
        minus_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verbrauch_minus(curr_day);
            }
        });
        //Anzeigen Diagramm
        chart_create(curr_day);
    }

    private void verbrauch_minus(int curr_day) {
        //Abhängig vom Tag wird der Verbrauch reduziert
        switch (curr_day){
            case Calendar.MONDAY:
                verbrauch_mo = verbrauch_mo - value;
                break;
            case Calendar.TUESDAY:
                verbrauch_di = verbrauch_di - value;
                break;
            case Calendar.WEDNESDAY:
                verbrauch_mi = verbrauch_mi - value;
                break;
            case Calendar.THURSDAY:
                verbrauch_do = verbrauch_do - value;
                break;
            case Calendar.FRIDAY:
                verbrauch_fr = verbrauch_fr - value;
                break;
            case Calendar.SATURDAY:
                verbrauch_sa = verbrauch_sa - value;
                break;
            case Calendar.SUNDAY:
                verbrauch_so = verbrauch_so - value;
                break;
        }
        chart_create(curr_day);
    }

    //Diagramm erstellen
    private void chart_create(int curr_day){
        //Bar Chart
        BarChart chart = findViewById(R.id.barchart);

        ArrayList day_verbrauch = new ArrayList();
        ArrayList day = new ArrayList();
        //Abhängig vom Tag wird das Diagramm anders dargestellt
        switch (curr_day){
            case Calendar.MONDAY:
                day_verbrauch.add(new BarEntry(verbrauch_di, 0));
                day_verbrauch.add(new BarEntry(verbrauch_mi, 1));
                day_verbrauch.add(new BarEntry(verbrauch_do, 2));
                day_verbrauch.add(new BarEntry(verbrauch_fr, 3));
                day_verbrauch.add(new BarEntry(verbrauch_sa, 4));
                day_verbrauch.add(new BarEntry(verbrauch_so, 5));
                day_verbrauch.add(new BarEntry(verbrauch_mo, 6));
                day.add("Di");
                day.add("Mi");
                day.add("Do");
                day.add("Fr");
                day.add("Sa");
                day.add("So");
                day.add("Mo");
                break;
            case Calendar.TUESDAY:
                day_verbrauch.add(new BarEntry(verbrauch_mi, 0));
                day_verbrauch.add(new BarEntry(verbrauch_do, 1));
                day_verbrauch.add(new BarEntry(verbrauch_fr, 2));
                day_verbrauch.add(new BarEntry(verbrauch_sa, 3));
                day_verbrauch.add(new BarEntry(verbrauch_so, 4));
                day_verbrauch.add(new BarEntry(verbrauch_mo, 5));
                day_verbrauch.add(new BarEntry(verbrauch_di, 6));
                day.add("Mi");
                day.add("Do");
                day.add("Fr");
                day.add("Sa");
                day.add("So");
                day.add("Mo");
                day.add("Di");
                break;
            case Calendar.WEDNESDAY:
                day_verbrauch.add(new BarEntry(verbrauch_do, 0));
                day_verbrauch.add(new BarEntry(verbrauch_fr, 1));
                day_verbrauch.add(new BarEntry(verbrauch_sa, 2));
                day_verbrauch.add(new BarEntry(verbrauch_so, 3));
                day_verbrauch.add(new BarEntry(verbrauch_mo, 4));
                day_verbrauch.add(new BarEntry(verbrauch_di, 5));
                day_verbrauch.add(new BarEntry(verbrauch_mi, 6));
                day.add("Do");
                day.add("Fr");
                day.add("Sa");
                day.add("So");
                day.add("Mo");
                day.add("Di");
                day.add("Mi");
                break;
            case Calendar.THURSDAY:
                day_verbrauch.add(new BarEntry(verbrauch_fr, 0));
                day_verbrauch.add(new BarEntry(verbrauch_sa, 1));
                day_verbrauch.add(new BarEntry(verbrauch_so, 2));
                day_verbrauch.add(new BarEntry(verbrauch_mo, 3));
                day_verbrauch.add(new BarEntry(verbrauch_di, 4));
                day_verbrauch.add(new BarEntry(verbrauch_mi, 5));
                day_verbrauch.add(new BarEntry(verbrauch_do, 6));
                day.add("Fr");
                day.add("Sa");
                day.add("So");
                day.add("Mo");
                day.add("Di");
                day.add("Mi");
                day.add("Do");
                break;
            case Calendar.FRIDAY:
                day_verbrauch.add(new BarEntry(verbrauch_sa, 0));
                day_verbrauch.add(new BarEntry(verbrauch_so, 1));
                day_verbrauch.add(new BarEntry(verbrauch_mo, 2));
                day_verbrauch.add(new BarEntry(verbrauch_di, 3));
                day_verbrauch.add(new BarEntry(verbrauch_mi, 4));
                day_verbrauch.add(new BarEntry(verbrauch_do, 5));
                day_verbrauch.add(new BarEntry(verbrauch_fr, 6));
                day.add("Sa");
                day.add("So");
                day.add("Mo");
                day.add("Di");
                day.add("Mi");
                day.add("Do");
                day.add("Fr");
                break;
            case Calendar.SATURDAY:
                day_verbrauch.add(new BarEntry(verbrauch_so, 0));
                day_verbrauch.add(new BarEntry(verbrauch_mo, 1));
                day_verbrauch.add(new BarEntry(verbrauch_di, 2));
                day_verbrauch.add(new BarEntry(verbrauch_mi, 3));
                day_verbrauch.add(new BarEntry(verbrauch_do, 4));
                day_verbrauch.add(new BarEntry(verbrauch_fr, 5));
                day_verbrauch.add(new BarEntry(verbrauch_sa, 6));
                day.add("So");
                day.add("Mo");
                day.add("Di");
                day.add("Mi");
                day.add("Do");
                day.add("Fr");
                day.add("Sa");
                break;
            case Calendar.SUNDAY:
                day_verbrauch.add(new BarEntry(verbrauch_mo, 0));
                day_verbrauch.add(new BarEntry(verbrauch_di, 1));
                day_verbrauch.add(new BarEntry(verbrauch_mi, 2));
                day_verbrauch.add(new BarEntry(verbrauch_do, 3));
                day_verbrauch.add(new BarEntry(verbrauch_fr, 4));
                day_verbrauch.add(new BarEntry(verbrauch_sa, 5));
                day_verbrauch.add(new BarEntry(verbrauch_so, 6));
                day.add("Mo");
                day.add("Di");
                day.add("Mi");
                day.add("Do");
                day.add("Fr");
                day.add("Sa");
                day.add("So");
                break;
        }

        chart.setDescription("");    // Hide the description
        chart.getAxisLeft().setDrawLabels(false);
        chart.getAxisRight().setDrawLabels(false);
        //chart.getXAxis().setDrawLabels(false);
        chart.getLegend().setEnabled(false);   // Hide the legend
        chart.setScaleEnabled(false); //Zoomen deaktiveren
        chart.setHighlightPerTapEnabled(false);

        BarDataSet bardataset = new BarDataSet(day_verbrauch, "Day");
        //chart.animateY(1500);
        BarData data = new BarData(day, bardataset);
        bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
        chart.setData(data);
        //Refresh Diagramm
        chart.notifyDataSetChanged();
        chart.invalidate();
    }
}




