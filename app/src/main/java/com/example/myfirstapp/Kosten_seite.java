package com.example.myfirstapp;

import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class Kosten_seite extends AppCompatActivity {

    public static final String SHARED_PREFS = "sharedPrefs";

    //Kosten pro Typ in Euro
    private float wasser_kosten = 100f;
    private float strom_kosten = 300f;
    private float gas_kosten = 150f;
    private float heizung_kosten = 200f;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kosten_seite);




        loadData();


        //Pie Chart
        PieChart pieChart = findViewById(R.id.piechart);
        ArrayList verbrauchart = new ArrayList();


        if(wasser_kosten !=0)
        verbrauchart.add(new Entry(wasser_kosten, 0));
        if(strom_kosten !=0)
        verbrauchart.add(new Entry(strom_kosten, 1));
        if(gas_kosten !=0)
        verbrauchart.add(new Entry(gas_kosten, 2));
        if(heizung_kosten !=0)
        verbrauchart.add(new Entry(heizung_kosten, 3));


        PieDataSet dataSet = new PieDataSet(verbrauchart, "");

        ArrayList kosten = new ArrayList();

        if(wasser_kosten !=0)
        kosten.add("Wasser");
        if(strom_kosten !=0)
        kosten.add("Strom");
        if(gas_kosten !=0)
        kosten.add("Gas");
        if(heizung_kosten !=0)
        kosten.add("Heizung");


        PieData data = new PieData(kosten, dataSet);
        pieChart.setData(data);
       dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        pieChart.animateXY(1500, 1500);

    }

    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);

        strom_kosten = sharedPreferences.getFloat("stromkosten", 10f);
        gas_kosten =  sharedPreferences.getFloat("gaskosten", 10f);
        heizung_kosten =  sharedPreferences.getFloat("heizungkosten", 10f);
        wasser_kosten =  sharedPreferences.getFloat("wasserkosten", 10f);
    }
}
