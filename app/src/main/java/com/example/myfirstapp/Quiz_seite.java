package com.example.myfirstapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class Quiz_seite extends AppCompatActivity {

    //-------------------- Class Frage ------------------
    public class Frage{
        String Frage_string;
        boolean wahr_falsch;
        int xp;

        Frage(){
            Frage_string ="Testfrage";
            wahr_falsch = true;
            xp = 10;
        }
        Frage(String Frage,boolean Ist_die_Frage_wahr,int Wie_viel_xp_bekommt_man){
            Frage_string = Frage;
            wahr_falsch = Ist_die_Frage_wahr;
            xp  = Wie_viel_xp_bekommt_man;
        }
        public String getFrage_string(){
            return Frage_string;
        }
        public int getXp(){
            return xp;
        }
        public boolean getWahr_falsch(){
            return wahr_falsch;
        }
    }



    ArrayList<Frage> fragen = new ArrayList<>();

    Frage frage1 = new Frage("Ist Stromsparen gut?",true,5);
    Frage frage2 = new Frage("Fernsehen und PC spielen hilft Strom zu sparen!",false,10);
    Frage frage3 = new Frage("Entstehen bei der Energieerzeugung mit fossilen Brennstoffen (Öl, Kohle, Gas) Abgase?",true,12);
    Frage frage4 = new Frage("Ein Kühlschrank der Energieeffizienzklasse A++ ist um 50% sparsamer als ein Gerät der Klasse A!",true,10);
    Frage frage5 = new Frage("Eine Energiesparlampe hält nicht so lange wie eine normale Lampe!",false,12);
    Frage frage6 = new Frage("Plasmafernseher brauchen am meisten Strom!",true,10);
    Frage frage7 = new Frage("Die Heizung eines Hauses macht 20 Prozent des Energieverbrauchs aus!",false,10);


    // ------------ Random ----------------
    Random fragen_rnd = new Random();

    //--------------------- Textview ------------------
    private TextView Ausgabe_Frage;

    //--------------------- Button -----------------------
    private ImageButton richtig;
    private ImageButton falsch;

    //---------------------Variablen ---------------------
    private int ErfahrungspunkteInsgesamt = 0;
    private int Erfahrungspunkte = 0;
    private int level = 1;
    private int levelfueraufstieg = 50; //Um die Progressbar-Länge je nach Level zu erhöhen.
    private Frage akt_frage;
    private int akt_frage_idx;
    private int new_akt_frage_idx;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_seite);
        fragen.add(frage1);
        fragen.add(frage2);
        fragen.add(frage3);
        fragen.add(frage4);
        fragen.add(frage5);
        fragen.add(frage6);
        fragen.add(frage7);

        akt_frage_idx = fragen_rnd.nextInt(fragen.size());

        akt_frage = fragen.get(akt_frage_idx);

        loadData();


        Ausgabe_Frage = findViewById(R.id.textView_Frage_quiz);

        richtig = findViewById(R.id.imageButton_richtig);
        falsch = findViewById(R.id.imageButton_falsch);

        Ausgabe_Frage.setText(akt_frage.Frage_string);

        //------------------------ OnClickListender für Richtig-Button -------------------
        richtig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                richtig_check(akt_frage.wahr_falsch);
            }
        });

        //------------------------- OnClickListender für FalschButton ---------------------
        falsch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                falsch_check(akt_frage.wahr_falsch);
            }
        });
    }

    //---------------Richtig-Button check----------------
    private void richtig_check(boolean a){
        if(a == true){
            Toast toast = Toast.makeText(this,"+ "+ akt_frage.xp +" xp",Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            Erfahrungspunkte += akt_frage.xp;
            if(Erfahrungspunkte >= levelfueraufstieg){
                level++;
                Erfahrungspunkte = Erfahrungspunkte-levelfueraufstieg;
            }
            ErfahrungspunkteInsgesamt += akt_frage.xp;
        }
        else{
            Toast toast = Toast.makeText(this,"Leider Falsch!",Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        saveXp();
        do {
            new_akt_frage_idx = fragen_rnd.nextInt(fragen.size());
        }while (akt_frage_idx == new_akt_frage_idx);
        akt_frage_idx = new_akt_frage_idx;
        akt_frage = fragen.get(akt_frage_idx);
        Ausgabe_Frage.setText(akt_frage.Frage_string);
    }

    // --------------Falsch-Button check ----------------
    private  void falsch_check(boolean a){
        if(a == false){
            Toast toast = Toast.makeText(this,"+ "+ akt_frage.xp +" xp",Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            Erfahrungspunkte += akt_frage.xp;
            if(Erfahrungspunkte >= levelfueraufstieg){
                level++;
                Erfahrungspunkte = Erfahrungspunkte-levelfueraufstieg;
                levelfueraufstieg+=50;
            }
            ErfahrungspunkteInsgesamt += akt_frage.xp;
        }
        else{
            Toast toast = Toast.makeText(this,"Leider Falsch!",Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
        saveXp();
        do {
            new_akt_frage_idx = fragen_rnd.nextInt(fragen.size());
        }while (akt_frage_idx == new_akt_frage_idx);
        akt_frage_idx = new_akt_frage_idx;
        akt_frage = fragen.get(akt_frage_idx);
        Ausgabe_Frage.setText(akt_frage.Frage_string);
    }

    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences("sharedPrefs",MODE_PRIVATE);
        Erfahrungspunkte = sharedPreferences.getInt("Erfahrungspunkte",0);
        ErfahrungspunkteInsgesamt = sharedPreferences.getInt("ErfahrungspunkteInsgesamt",0);
        levelfueraufstieg = sharedPreferences.getInt("levelfueraufstieg",50);
        level = sharedPreferences.getInt("level",1);
    }

    private void saveXp(){
        SharedPreferences sharedPreferences = getSharedPreferences("sharedPrefs",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("ErfahrungspunkteInsgesamt",ErfahrungspunkteInsgesamt);
        editor.putInt("Erfahrungspunkte",Erfahrungspunkte);
        editor.putInt("level",level);
        editor.putInt("levelfueraufstieg", levelfueraufstieg);
        editor.apply();
    }

    //---------------------- Wenn der Backbutten gedrückt wird. ------------------------
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,Startseite.class);
        startActivity(intent);
        super.onBackPressed();
    }
}
